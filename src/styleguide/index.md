# General Principles

We mainly use Sass (written as SCSS) as a pre-processor to generate the final CSS styles for each project. As a result, our CSS coding style mixes with the Sass and on the following document we try to give this distinction by pointing to what only applies to Sass (in general because it is invalid CSS).

> A coding styleguide (note, not a visual styleguide) is a valuable tool for teams who:
> * build and maintain products for a reasonable length of time;
> * have developers of differing abilities and specialties;
> * have a number of different developers working on a product at any given time;
> * on-board new staff regularly;
> * have a number of codebases that developers dip in and out of.

1. Be consistant with indentation
2. Be consistant about where spaces before/after colons/braces go
3. One selector per line, One rule per line
4. List related properties together
5. Have a plan for class name naming
6. Don't use ID's

---

## Format

* Use soft-tabs with a two space indent. Spaces are the only way to guarantee code renders the same in any person’s environment.
* Include one space before the opening braces.

```scss
/* Bad */
.selector-1,
.selector-2{
  /* Declarations */
}

/* Good */
.selector-1,
.selector-2 {
  /* Declarations */
}
```

* Opening braces on the same line as the last selector in the list.

```scss
/* Bad */
.selector-1,
.selector-2
{
  /* Declarations */
}

/* Good */
.selector-1,
.selector-2 {
  /* Declarations */
}
```

* Closing braces on a different line.

```scss
/* Bad */
.selector-1,
.selector-2 {
  /* Declarations */ }

/* Good */
.selector-1,
.selector-2 {
  /* Declarations */
}
```

* Separate each rule set with a single blank line.

```scss
/* Bad */
.selector-1 {
  /* Declarations */
}
.selector-2 {
  /* Declarations */
}

/* Good */
.selector-1 {
  /* Declarations */
}

.selector-2 {
  /* Declarations */
}
```

* Use double quotes instead of single quotes.

```scss
/* Bad */
.selector {
  background-image: url('images/half-quote.jpg');
  content: '';
}

/* Good */
.selector {
  background-image: url("images/half-quote.jpg");
  content: "";
}
```

* Include one space on comma-separated values, after each comma.

```scss
/* Bad */
.selector-1 {
  background-image: url("images/my-lovely-face.png"),url("images/my-pink-background.png");
}

.selector-2 {
  background-image: linear-gradient(to top,yellow,pink);
  color: rgba(32,32,33,.5);
}

/* Good */
.selector-1 {
  background-image: url("images/my-lovely-face.png"), url("images/my-pink-background.png");
}

.selector-2 {
  background-image: linear-gradient(to top, yellow, pink);
  color: rgba(31, 32, 33, .5);
}
```

* Use hex color codes #000 unless using rgba() in raw CSS. Although it's not much, it allows for a given color to be specified with less characters, reducing the final file size without any loss of specificity as both method map the same color space. (Note: SCSS’ rgba() function is overloaded to accept hex colors as a parameter.)

```scss
/* Bad */
.selector {
    background-color: rgba(255, 221, 102, .8);
    color: rgb(0, 136, 255);
}

/* Good */
.selector {
    background-color: rgba(255, 221, 102, .8);
    color: #08f;
}

// SCSS
.selector {
    background-color: rgba(#fd6, .8);
}
```

## Selectors

* Write one selector per line.

```scss
/* Bad */
.selector-1, .selector-2, .selector-3 {
  /* Declarations */
}

/* Good */
.selector-1,
.selector-2,
.selector-3 {
  /* Declarations */
}
```

* Quote attribute values in selectors.

```scss
/* Bad */
input[type=checkbox] {
  /* Declarations */
}

/* Good */
input[type="checkbox"] {
  /* Declarations */
}
```

* Never use IDs, they aren't re-usable.

/* Bad */
#content {
  /* Declarations */
}

/* Good */
.content {
  /* Declarations */
}

* Pseudo-classes should be declared with a single colon.

```scss
/* Bad */
.selector::hover {
    /* Declarations */
}

/* Good */
.selector:hover {
    /* Declarations */
}
```

* Pseudo-elements should be declared with a double colon.

```scss
/* Bad */
.selector:before {
    /* Declarations */
}

/* Good */
.selector::before {
    /* Declarations */
}
```

## Declarations

* Write one declaration per line.

```scss
/* Bad */
.selector {
  color: #fff; text-align: center; width: 20px;
}

/* Good */
.selector {
  color: #fff;
  text-align: center;
  width: 20px;
}
```

* Include one space before each declaration value.

```css
/* Bad */
.selector {
  color:#fff;
  text-align:center;
  width:20px;
}

/* Good */
.selector {
  color: #fff;
  text-align: center;
  width: 20px;
}
```

* All declarations must end with a semi-colon, even the last one, to avoid error.

```css
/* Bad */
.selector {
  color: #fff;
  text-align: center;
  width: 20px
}

/* Good */
.selector {
  color: #fff;
  text-align: center;
  width: 20px;
}
```

* Use one level of indentation for each declaration.

```css
/* Bad */
.selector {
  color: #fff;
    text-align: center;
    width: 20px;
}

/* Bad */
.selector {
color: #fff;
text-align: center;
width: 20px;
}

/* Good */
.selector {
  color: #fff;
  text-align: center;
  width: 20px;
}
```

* Declarations should be ordered alphabetically.

```css
/* Example */
.selector {
  background-color: #000;
  background-image: url("images/bg.jpg");
  background-position: center;
  background-repeat: no-repeat;
  border: 10px solid #333;
  box-sizing: border-box;
  display: inline-block;
  font-family: sans-serif;
  font-size: 16px;
  height: 100px;
  margin: 10px;
  overflow: hidden;
  padding: 10px;
  width: 100px;
}
```

* Don't use shorthand notations to set only one value.

```css
/* Bad */
.selector {
  background: blue;
}

/* Good */
.selector {
  background-color: blue;
}
```

* Zero values for length dimensions don't need units. (If we are talking about time, put the units in there, some browsers deem the declaration invalid otherwise.)

```css
/* Bad */
.selector {
  margin: 0rem;
}

/* Good */
.selector {
  margin: 0;
}
```

* Line height should also be unit-less, unless it is required to be a specific value for layout purposes. (The fact that line height can accept unit-less values and that this should be preferable is kind of old, [2006 old](http://meyerweb.com/eric/thoughts/2006/02/08/unitless-line-heights/).)

```css
/* Acceptable */
.header {
  font-size: 1.2rem;
  line-height: 60px;
}

/* Default */
.selector {
  font-size: .9rem;
  line-height: 1.2;
}
```

* Write lowercase values, except for font names.
    * Use shorthand hex values.

```css
/* Bad */
.selector {
  background-color: #FFFFFF;
  font-family: "Helvetica Neue", sans-serif;
}

/* Good */
.selector {
  background-color: #fff;
  font-family: "Helvetica Neue", sans-serif;
}
```

* Long, comma-separated property values (eg. gradients or shadows) can be arranged across multiple lines in an effort to improve readability and produce more useful diffs. In this case:
    * All values should be in a new line
    * Indentation of each value must be one level deeper than the declaration
    * Commas should be at the end of each property
    * The last property must end with the declaration's semi-colon

```css
/* Acceptable */
.selector {
  box-shadow: 0 2px 2px #ccc, 0 -2px 2px #ccc;
}

/* Bad */
.selector {
  box-shadow: 0 2px 2px #ccc,
  0 -2px 2px #ccc;
}

/* Good */
.selector {
  box-shadow:
    0 2px 2px #ccc,
    0 -2px 2px #ccc;
}
```

* Strenuously avoid declaring the `height` property, prefer the use of min-height. When it can't be avoided, consider what happens if somehow the content gets bigger than the declared height.

```css
/* Bad */
.selector {
    display: inline-block;
    height: 50px;
}

/* Good */
.selector {
    display: inline-block;
    min-height: 50px;
}
```

* Never use `!important`.

```css
/* Bad */
.selector {
    color: red !important;
}
```

## Media Queries

All the `@media` rulesets should be placed after each rule with the declarations that override/extend that rule.

```css
/* Example */
.select-1 {
    color: #000;
    text-align: center;
}

@media (min-width: 800px) {
    .select-1 {
        text-align: left;
    }
}

.selector-2 {
    display: block;
    width: 100%;
}

@media (min-width: 800px) {
    .selector-2 {
        display: inline-block;
        width: 30%;
    }
}
```

Default styles should be mobile-first, with variations/corrections added with media queries conditioned with `min-width`. However, the use of `max-width` is encouraged if it results in easier code to read and maintain.

```css
/* Example */
@media (max-width: 480px) {
    .mobile-prevent-scroll {
        /* Declarations */
    }
}
```

## Naming Classes

We follow the general principles of [Block Element Variation Modifier (BEVM)](https://www.viget.com/articles/bem-sass-modifiers) (which is in it self a different take on [Block Element Modifier (BEM)](https://en.bem.info/)).

We support both boolean modifiers and key-value modifiers. Also, we don't use multiple word modifiers, either for their names or their values.

To define our BEVM entities we use the following string format:
```
block[--variation][__element[--variation]] -modifier
```

Rules for each part of the string:
* `block` and `__element` - The name of the block and element, respectively. Can be a double word name with a hyphen separator.
* `--variation` - Block and element variation identifier. Must be a single word.
* `-modifier` - Modifier can be a single word, boolean modifier, or two words in a key-value style.

Available selector variations:
* `block` - The block.
* `block--variation` - Variation of the block.
* `block__element` - The element inside the block.
* `block--variation__element` The element inside the variation of the block.
* `block__element--variation` - Variation of the element inside the block.
* `block--variation__element--variation` - Variation of the element inside the variation of the block.
* `-modifier` - A modifier to apply to any of the above selectors.

### Modifiers

In this naming convention, modifiers are detached from the main selector so we don't get a very long and repetitive set of classes for a certain element.

```html
<!-- This BEM -->
<button class="button button--success button-large button-block"></button>

<!-- Becomes this -->
<button class="button -success -large -block"></button>
```

However, modifiers must be styled attached to another selector and never as a first level selector. This allows for a modifier with the same name to do different things on different elements/blocks, without having to override any other properties.

```scss
// Bad
.-modifier {
    color: red;
    font-size: 2em;
}

// Good
.block__element.-modifier {
    color: red;
    font-size: 2em;
}
```

### Variations

We should avoid the use of variation on small projects, opting for a modifier only approach. Also, even on big project the first modules should be constructed and extended/modified using modifiers, the use of variations should came later on to allow simpler selectors and rules when using modifiers will cause more confusion.

As a guiding rule, if it makes your life simpler and the code more easy to read, use a variation.

```scss
// Example: buttons
// Without variations
.button {
    background-color: white;
    border-radius: 3px;
    color: black;
    padding: 10px;

    .-success {
        background-color: green;
        color: white;
    }

    .-error {
        background-color: red;
        color: white;
    }

    .-primary {
        background-color: blue;
        color: white;
    }
}

// With variations
%button-default {
    border-radius: 3px;
    padding: 10px;
}

.button--success {
    @extend %button-default;
    background: green;
    color: white;
}

.button--error {
    @extend %button-default;
    background: red;
    color: white;
}

.button--primary {
    @extend %button-default;
    background: blue;
    color: white;
}
```

### State Classes

A state is something that augments the current module in a very specific way, not necessarily in the properties it defines, but in the resulting output. Forms are a particular example where there will be state classes for sure. When a form field has errors or the field was filled successfully classes like `.is-success` or `.is-error` will bring styles to the element highlighting this state.

These classes, given that their job is to force a given style over everything else, may use `!important`, but only if there is no other way to be sure their styles will always override the ones from the element. When, using `!important`, there must be a comment stating the reason.

* Never create a ruleset with a selector composed of just the State Class, this makes it so there are no "default" properties for that class and avoids problems if (god forbid) the need to use `!important` arises.
* Avoid, with all your might, the use of `!important` in the declarations.
* Always comment the declarations where you failed to not use `!important`.

```css
/* Bad */
.is-error {
    border-color: red;
    color: red !important;
}

/* Good */
.selector.is-error {
    border-color: red;
    color: red !important; /* Very important reason. */
}
```

### Interaction Classes

Class names starting with `js-` (e.g. `.js-search-toggle`). **These classes must never be styled**, they serve as bindings for JavaScript events and allow styles to be completely changed, including class names, without any loss of functionality.

```css
/* NEVER DO THIS */
.js-search-toggle {
    background-color: pink;
    border-radius: 3px;
    color: white;
    font-weight: bold;
    padding: 10px;
}
```

## SCSS

In writing SCSS (where all files end with a **`.scss`** and not a `.sass`), the above and all that follows, should be taken into account. If the guideline seems duplicated follow the one from the SCSS section, for everything that's not mentioned fallback to the CSS guidelines.

### Declaration Order

1. `$variable` (local overrides or private)
2. `@extend`
3. `@include`
4. Regular declarations (allows for overriding)
5. Mixins with content blocks (`@media` declarations included here)
6. Selectors that target itself:
    1. pseudo-classes (like `:hover` or `:first-child`)
    2. pseudo-elements (like `::after` and `::before`)
    3. component states (anything like `&.pretty-classname`)
5. Nested declarations

Each of these items should be separated from the next by a blank line.

### Numbers

In Sass, a number is a data type including everything from unit-less numbers to lengths, durations, frequencies, angles and so on. This allows for complex calculations which may be very helpful, but as with any great power comes great danger.

#### Units

Given the mathematical powers of Sass, units are not mere strings attached to an integer/float value, they are more like algebraic variables and will also suffer mathematical operations.

* Don't use units in intermediary calculations, this avoids any adverse outcomes in your results.
* Don't print/use your final values by concatenating a string with the units, multiply by one unit of the units you'd like.

```scss
$value: 42;

// Bad
$length: $value + px;

// Good
$length: $value * 1px;
```

* Removing units is a simple task as well, you just need to divide by one unit of the same type. (This is just an example, you should never use units in intermediary calculations.)

```scss
$length: 42px;

// Bad
$value: str-slice($length + unquote(''), 1, 2);

// Good
$value: $length / 1px;
```

#### Calculations

For improved readability, wrap all math operations in parentheses with a single space between values, variables, and operators. This also serves to force Sass to evaluate the contents of the parentheses.

```scss
// Bad example
.element {
  margin: 10px 0 @variable*2 10px;
}

// Good example
.element {
  margin: 10px 0 (@variable * 2) 10px;
}
```

### Nesting

While nesting is great, too much of it can make the code harder to read than plain boring ol' CSS. Also, it creates excessive over specificity which results in greater specificity when overriding styles. As a rule, nesting should be avoided as much as possible.

* Nesting should not be **deeper than 3 levels**. If it can't be helped, step back and rethink the overall strategy.

* **Nesting required** when it always makes the code easier to read
    * pseudo-classes
    * pseudo-elements
    * component states
    * media queries

* **Nesting can be used** on some coding styles like [RSCSS](https://github.com/rstacruz/rscss), and in our case we use nesting for [Modifiers](#modifiers) and [State Classes](#state-classes).

### Functions

When using functions the main problem is avoiding conflict with the default CSS functions and, given a complex project, navigating through our own functions.

* All functions must start with a hyphen `-` prefixing their name: `-function`. This avoids any conflict with the default functions, in case we happen to need the same name (not advised).
* Prefix the functions with a namespace: `-ns-function`; if the project grows to have more than a handful of functions or you absolutely need to use the same name as another CSS function (avoid this at all costs).
* Functions should try to do only one thing.

### Mixins

* Use mixins only when there are dynamic properties, otherwise prefer the use of `@extend` and placeholders.
* Avoid using more than 4 parameters. It is a sign that a mixin is too complex.

### File structure

* Each logical module of code should live in its own file. Avoiding multiple objects in the same file allows for the use of the filesystem as a means to navigate the styles rather than relying on comments.
* The following should always have a file of their own:
    * Variables
    * Functions
    * Mixins
    * Placeholders
* Files should be named for easy grasp of their contents
* All files should be partials, their name should start with an underscore `_`, except if the file is processed to a CSS file
* If possible the files that generate the compiled CSS should never have anything other than `@import` statements and comments

#### Folder Structure

In most projects multiple Functions and Mixins will for sure exist. Considering this, the following structure should be the backbone for any project:

```
/styles
|--/vendor
|--/functions
|  |-- _calc-awesome.scss
|   -- _catch-fire.scss
|--/mixins
|  |-- _button.scss
|  |-- _media-queries.scss
|   -- _positioning
|-- _variables.scss
 -- main.scss
```

* `vendor/`: contains all of the CSS from outside your codebase, usually relating to JavaScript dependencies.
* `functions/`: contains all of your Sass functions.
* `mixins/`: contains all of your Sass mixins.
* `_variables.scss`: contains all of your global variables.
* `main.scss`: used to import everything needed to generate the output CSS file.

## Resources

* [Principles of writing consistent, idiomatic CSS](https://github.com/necolas/idiomatic-css)
* [Principles for writing consistent, clean, friendly Sass](https://github.com/anthonyshort/idiomatic-sass)
* [How we do CSS at Ghost](https://dev.ghost.org/css-at-ghost/)
* [Code Guide by @mdo](http://mdo.github.io/code-guide/#css)
* [BEM. Block Element Modifier](https://en.bem.info/)
* [Sass Guidelines](http://sass-guidelin.es)
* [Sassier (BE)Modifiers](https://www.viget.com/articles/bem-sass-modifiers)



# The Grid

<p class="lead">Problem: You've got tons of content, each needing different sized cells, and don't know how to quick and easily get it all done. Solution: The awesome XY grid!</p>

---

## Overview

The grid is built around two key elements: grid-x and cells. grid-container create a max-width and contain the grid, and cells create the final structure. Everything on your page that you don't give a specific structural style to should be within a grid-x or cell.

---


## Nesting

In the Grid you can nest cells down as far as you'd like. Just embed grid-x inside cells and go from there. Each embedded grid-x can contain up to 12 cells.

---

## How to Use

Using this framework is easy. Here's how your code will look when you use a series of `<div>` tags to create cells.

```html
<div class="grid-x">
  <div class="small-6 medium-4 large-3 cell">...</div>
  <div class="small-6 medium-8 large-9 cell">...</div>
</div>
```

<div class="grid-x display">
  <div class="small-12 large-4 cell">4</div>
  <div class="small-12 large-4 cell">4</div>
  <div class="small-12 large-4 cell">4</div>
</div>
<div class="grid-x display">
  <div class="small-12 large-3 cell">3</div>
  <div class="small-12 large-6 cell">6</div>
  <div class="small-12 large-3 cell">3</div>
</div>
<div class="grid-x display">
  <div class="small-12 large-2 cell">2</div>
  <div class="small-12 large-8 cell">8</div>
  <div class="small-12 large-2 cell">2</div>
</div>
<div class="grid-x display">
  <div class="small-12 large-3 cell">3</div>
  <div class="small-12 large-9 cell">9</div>
</div>
<div class="grid-x display">
  <div class="small-12 large-4 cell">4</div>
  <div class="small-12 large-8 cell">8</div>
</div>
<div class="grid-x display">
  <div class="small-12 large-5 cell">5</div>
  <div class="small-12 large-7 cell">7</div>
</div>
<div class="grid-x display">
  <div class="small-12 large-6 cell">6</div>
  <div class="small-12 large-6 cell">6</div>
</div>

---

## Nesting grid-x

In the Grid you can nest cells down as far as you'd like. Just embed grid-x inside cells and go from there. Each embedded grid-x can contain up to 12 cells.

```html
<div class="grid-x">
  <div class="small-8 cell">8
    <div class="grid-x">
      <div class="small-8 cell">8 Nested
        <div class="grid-x">
          <div class="small-8 cell">8 Nested Again</div>
          <div class="small-4 cell">4</div>
        </div>
      </div>
      <div class="small-4 cell">4</div>
    </div>
  </div>
  <div class="small-4 cell">4</div>
</div>
```

<div class="grid-x display">
  <div class="small-8 cell">8
    <div class="grid-x">
      <div class="small-8 cell">8 Nested
        <div class="grid-x">
          <div class="small-8 cell">8 Nested Again</div>
          <div class="small-4 cell">4</div>
        </div>
      </div>
      <div class="small-4 cell">4</div>
    </div>
  </div>
  <div class="small-4 cellgi">4</div>
</div>

---

## Small Grid

As you've probably noticed in the examples above, you have access to a small, medium, and large grid. If you know that your grid structure will be the same for small devices as it will be on large devices, just use the small grid. You can override your small grid classes by adding medium or large grid classes.

```html
<div class="grid-x">
  <div class="small-2 cell">2</div>
  <div class="small-10 cell">10, last</div>
</div>
<div class="grid-x">
  <div class="small-3 cell">3</div>
  <div class="small-9 cell">9, last</div>
</div>
```

<div class="grid-x display">
  <div class="small-2 cell">2</div>
  <div class="small-10 cell">10, last</div>
</div>
<div class="grid-x display">
  <div class="small-3 cell">3</div>
  <div class="small-9 cell">9, last</div>
</div>



# Colors

<p class="lead">Below you can find the different values we created that support the primary color variable you can change at any time in <code>\_settings.scss</code></p>

---

<div class="row up-1 medium-up-3 large-up-5">
  <div class="column">
    <div class="color-block">
      <span style="background: #d13528"></span>
      #d13528
    </div>
  </div>
  <div class="column">
    <div class="color-block">
      <span style="background: #eceff1"></span>
      #eceff1
    </div>
  </div>
  <div class="column">
    <div class="color-block">
      <span style="background: #3adb76"></span>
      #3adb76
    </div>
  </div>
  <div class="column">
    <div class="color-block">
      <span style="background: #ffae00"></span>
      #ffae00
    </div>
  </div>
  <div class="column">
    <div class="color-block">
      <span style="background: #cc4b37"></span>
      #cc4b37
    </div>
  </div>
</div>



# Typography

<p class="lead">Worten uses Open Sans for headings and paragraph text.</p>

---

## Headings

Headings are used to denote different sections of content, usually consisting of related paragraphs and other HTML elements. They range from h1 to h6 and should be styled in a clear hierarchy (i.e., largest to smallest)

---

## Paragraphs

Paragraphs are groups of sentences, each with a lead (first sentence) and transition (last sentence). They are block level elements, meaning they stack vertically when repeated. Use them as such.

---

<h1>Heading Level 1</h1>

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic quibusdam ratione sunt dolorum, qui illo maxime doloremque accusantium cum libero eum, a optio odio placeat debitis ullam aut non distinctio.

<h2>Heading Level 2</h2>

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic quibusdam ratione sunt dolorum, qui illo maxime doloremque accusantium cum libero eum, a optio odio placeat debitis ullam aut non distinctio.

<h3>Heading Level 3</h3>

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic quibusdam ratione sunt dolorum, qui illo maxime doloremque accusantium cum libero eum, a optio odio placeat debitis ullam aut non distinctio.

<h4>Heading Level 4</h4>

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic quibusdam ratione sunt dolorum, qui illo maxime doloremque accusantium cum libero eum, a optio odio placeat debitis ullam aut non distinctio.

<h5>Heading Level 5</h5>

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic quibusdam ratione sunt dolorum, qui illo maxime doloremque accusantium cum libero eum, a optio odio placeat debitis ullam aut non distinctio.

<h6>Heading Level 6</h6>

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic quibusdam ratione sunt dolorum, qui illo maxime doloremque accusantium cum libero eum, a optio odio placeat debitis ullam aut non distinctio.



# Buttons

<p class="lead">Buttons are tied to an action of some kind, whether that button is on a cheese dispenser or launches the rocket that you're strapped to. On the web, we follow similar conventions.</p>

---

## Primary Buttons

These buttons are primary calls to action and should be used sparingly. Their size can be adjusted with the `.tiny`, `.small`, and `.large` classes.

```html_example
<a href="#" class="btn-primary large button">Large button</a>
<a href="#" class="btn-primary button">Regular button</a>
<a href="#" class="btn-primary small button">Small button</a>
<a href="#" class="btn-primary tiny button">Tiny button</a>
```

---

## Secondary Buttons

These buttons are used for less important, secondary actions on a page.

```html_example
<a href="#" class="btn-secondary large button">Large button</a>
<a href="#" class="btn-secondary button">Regular button</a>
<a href="#" class="btn-secondary small button">Small button</a>
<a href="#" class="btn-secondary tiny button">Tiny button</a>
```



# Top Bar

Top Bar element, used in all Worten Applications. Logo must have `90px * 20px` in white color without background.

```html_example
<header class="main-header">
  <div class="header_wrap">
    <div class="wser-topbar grid-x grid-margin-x main-nav align-middle">
        <div class="cell auto">
            <div class="grid-x grid-margin-x main-nav align-middle">
            <div class="logo">
                <a href="dashboard-page.html"><img class="logo-img" src="assets/img/logo.png"></a>
            </div>
            <div class="wser-menu">
                <ul class="dropdown menu" data-dropdown-menu>
                <li>
                    <a href="#">Parceiros</a>
                    <ul class="menu wser-submenu partners-menu">
                    <li>
                        <a href="#">
                        <span class="partner-logo">
                            <img src="assets/img/vodafone_logo.svg">
                        </span>
                        <span class="title">
                            Vodafone
                        </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <span class="partner-logo">
                            <img src="assets/img/nos_logo.png">
                        </span>
                        <span class="title">
                            NOS
                        </span>
                        </a>
                    <li>
                        <a href="#">
                        <span class="partner-logo">
                            <img src="assets/img/meo_logo.svg">
                        </span>
                        <span class="title">
                            MEO
                        </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <span class="partner-logo">
                            <img src="assets/img/galp_logo.png">
                        </span>
                        <span class="title">
                            Galp
                        </span>
                        </a>
                    </li>
                    </ul>
                </li>
                </ul>
            </div>
            </div>
        </div>
        <div class="cell shrink">
            <div class="grid-x grid-margin-x main-nav align-middle">
            <div class="search cell shrink">
                <i id="search-icon" class="icon search-white"></i>
            </div>
            <div class="user-block wser-menu cell shrink">
                <ul class="dropdown menu" data-dropdown-menu>
                <li>
                    <a href="#"><i id="user-icon" class="icon user"></i></a>
                    <ul class="menu wser-submenu">
                    <li><span class="title">Sessão iniciada como</span></li>
                    <li><span class="name">Parceiro - (Parceiro) Appproc1</span></li>
                    <hr>
                    <li><a href="1-preferencias.html">Preferências</a></li>
                    <li><a href="#">Terminar sessão</a></li>
                    </ul>
                </li>
                </ul>
            </div>
            <div class="shop topbar-item cell shrink">
                <span class="refer">L0874</span>
                <span class="name">WMB Colombo SA</span>
            </div>
            </div>
        </div>
        </div>

  </div>
</header>
```




# Tabular Data

```html_example
<div class="main-wrapper table-wrapper">
  <table class="ws-table">
    <thead>
      <tr>
        <th></th>
        <th>Nome</th>
        <th>NIF</th>
        <th>Telemóvel</th>
        <th>Nº Resolve</th>
        <th>Última Alteração</th>
        <th>Colaborador</th>
      </tr>
    </thead>
    <tbody>
      <tr class="clickable-row">
        <td><i class="icon alert"></i></td>
        <td><a href="client-page.html">João Faustino</a></td>
        <td>240244621</td>
        <td>+351 912 245 333</td>
        <td>92384984374</td>
        <td>07-12-2018, 15:45</td>
        <td>Parceiro - (Pareceiro) Appproc 2 <span>WMB Colombo SA</span></td>
      </tr>
      <tr class="clickable-row">
        <td><i class="icon alert"></i></td>
        <td><a href="client-page.html">João Ramalho</a></td>
        <td>240244621</td>
        <td>+351 913 398 287</td>
        <td>348734834734</td>
        <td>07-12-2018, 15:45</td>
        <td>Parceiro - (Pareceiro) Appproc 2 <span>WMB Colombo SA</span></td>
      </tr>
      <tr class="clickable-row">
        <td><i class="icon alert"></i></td>
        <td><a href="client-page.html">João Brás</a></td>
        <td>240244621</td>
        <td>+351 912 232 009</td>
        <td>38748734834</td>
        <td>07-12-2018, 15:45</td>
        <td>Parceiro - (Pareceiro) Appproc 2 <span>WMB Colombo SA</span></td>
      </tr>
      <tr class="clickable-row">
        <td><i class="icon alert"></i></td>
        <td><a href="client-page.html">João Leite</a></td>
        <td>240244621</td>
        <td>+351 967 746 203</td>
        <td>3847834748</td>
        <td>07-12-2018, 15:45</td>
        <td>Parceiro - (Pareceiro) Appproc 2 <span>WMB Colombo SA</span></td>
      </tr>
      <tr class="clickable-row">
        <td><i class="icon alert"></i></td>
        <td><a href="client-page.html">João Faustino</a></td>
        <td>240244621</td>
        <td>+351 912 245 333</td>
        <td>92384984374</td>
        <td>07-12-2018, 15:45</td>
        <td>Parceiro - (Pareceiro) Appproc 2 <span>WMB Colombo SA</span></td>
      </tr>
      <tr class="clickable-row">
        <td><i class="icon alert"></i></td>
        <td><a href="client-page.html">João Ramalho</a></td>
        <td>240244621</td>
        <td>+351 913 398 287</td>
        <td>348734834734</td>
        <td>07-12-2018, 15:45</td>
        <td>Parceiro - (Pareceiro) Appproc 2 <span>WMB Colombo SA</span></td>
      </tr>
    </tbody>
  </table>
</div>
```


# Forms

```html_example
<div class="container">
    <form>
        <h2 class="form-section-title">Dados do Cliente</h2>
        <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="medium-6 cell">
            <label>Nome
                <input type="text">
            </label>
            </div>
            <div class="medium-2 cell">
            <label>NIF
                <input type="text">
            </label>
            </div>
            <div class="medium-2 cell">
            <label>Telemóvel
                <input type="text">
            </label>
            </div>
            <div class="medium-2 cell">
            <label>Telefone
                <input type="text">
            </label>
            </div>
        </div>
        </div>
    </form>
</div>
```



# Dashboard
```html_example
<section class="dashboard slider">

  <div class="dashboard-element">
    <a href="client-list-energy.html">
      <div class="dashboard-element-wrapper">
        <h3 class="dashboard-title">Energia</h3>
        <img src="assets/img/dashboard-energy.png" alt="#">
        <div class="dashboard-values">
          <span>20<small>/20</small></span>
          <small>Contratos concluídos</small>
        </div>
        <div class="dashboard-progress-bars">
          <div class="dashboard-progress-el">
            <div class="dashboard-progress-header dashboard-meta-info">
              <span class="float-left">Contratos Pendentes</span>
              <span class="float-right">0</span>
            </div>
          </div>
          <div class="dashboard-progress-el">
            <div class="dashboard-progress-header dashboard-meta-info">
              <span class="float-left">Leads <i class="icon alert-red"></i></span>
              <span class="float-right">3/5</span>
            </div>
            <div class="progress-bar">
              <img src="assets/img/progress-bar1.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </a>
  </div>

  <div class="dashboard-element">
    <a href="client-list-communications.html">
      <div class="dashboard-element-wrapper">
        <h3 class="dashboard-title">Telecomunicações</h3>
        <img src="assets/img/dashboard-communication.png" alt="#">
        <div class="dashboard-values">
          <span>3<small>/10</small></span>
          <small>Contratos concluídos</small>
        </div>
        <div class="dashboard-progress-bars">
          <div class="dashboard-progress-el">
            <div class="dashboard-progress-header dashboard-meta-info">
              <span class="float-left">Contratos Pendentes</span>
              <span class="float-right">0</span>
            </div>
          </div>
          <div class="dashboard-progress-el">
            <div class="dashboard-progress-header dashboard-meta-info">
              <span class="float-left">Leads</span>
              <span class="float-right">8/9</span>
            </div>
            <div class="progress-bar">
              <img src="assets/img/progress-bar2.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </a>
  </div>

  <div class="dashboard-element">
    <a href="client-list-leasing.html">
      <div class="dashboard-element-wrapper">
        <h3 class="dashboard-title">Leasing</h3>
        <img src="assets/img/dashboard-leasing.png" alt="#">
        <div class="dashboard-values">
          <span>2<small>/15</small></span>
          <small>Contratos concluídos</small>
        </div>
        <div class="dashboard-progress-bars">
          <div class="dashboard-progress-el">
            <div class="dashboard-progress-header dashboard-meta-info">
              <span class="float-left">Contratos Pendentes</span>
              <span class="float-right">0</span>
            </div>
          </div>
          <div class="dashboard-progress-el">
            <div class="dashboard-progress-header dashboard-meta-info">
              <span class="float-left">Leads</span>
              <span class="float-right">12/12</span>
            </div>
            <div class="progress-bar">
              <img src="assets/img/progress-bar3.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </a>
  </div>

  <div class="dashboard-element">
    <a href="client-list-energy.html">
      <div class="dashboard-element-wrapper">
        <h3 class="dashboard-title">Energia</h3>
        <img src="assets/img/dashboard-energy.png" alt="#">
        <div class="dashboard-values">
          <span>20<small>/20</small></span>
          <small>Contratos concluídos</small>
        </div>
        <div class="dashboard-progress-bars">
          <div class="dashboard-progress-el">
            <div class="dashboard-progress-header dashboard-meta-info">
              <span class="float-left">Contratos Pendentes</span>
              <span class="float-right">0</span>
            </div>
          </div>
          <div class="dashboard-progress-el">
            <div class="dashboard-progress-header dashboard-meta-info">
              <span class="float-left">Leads <i class="icon alert-red"></i></span>
              <span class="float-right">3/5</span>
            </div>
            <div class="progress-bar">
              <img src="assets/img/progress-bar1.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </a>
  </div>

</section>
```
