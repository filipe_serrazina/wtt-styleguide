$(function(){

  let $cancelServiceBtn  = $('.js-new-service-bts .js-cancel-btn'),
      $selectTypeService = $('#type-service'),
      $closeService      = $('#close-service');

  $selectTypeService.change(function() {
    if($(this).val() != '') {
      $('.js-new-service-bts').show();
      $('html, body').animate({ scrollTop: 700 }, 1000);
      $('.type-service').removeClass('hide');
    } else {
      $('.js-new-service-bts').hide();
      $('.type-service').addClass('hide');
    }
  });

  $cancelServiceBtn.click(function() {
    $('.js-new-service-bts').hide();
    $('.type-service').addClass('hide');
    $('#type-service').val('');

    return false;
  });

  $closeService.click(function() {
    $('.service-1').addClass('hide');
  });
});
