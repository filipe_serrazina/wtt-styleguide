
$(function(){

  let $body             = $('body'),
      $backButton       = $('#back-btn'),
      $btnSearch        = $('.btn-search'),
      $instaSearchForm  = $('#insta-search-form'),
      $searchBox        = $('.searchbox'),
      $searchBtn        = $('#search-btn'),
      $searchIcon       = $('#search-icon'),
      $searchForm       = $('#searchbox'),
      $closeSearchBtn   = $('#close-search-btn'),
      $closeInstaSearch = $('#close-instasearch');

  $searchForm.keypress(function() {
    $btnSearch.removeClass('hide');
    $searchBox.css('width', '75%');
  });

  let $url = window.location.pathname.split('/').pop();

  if ($url != "2-new-client-edit.html" && $url != "new-service.html" && $url != "3-client-contract.html" && $url != "3.1-new-client-lead.html" && $url != "1-preferencias-edicao.html" && $url != "2-new-client-contract.html") {

    $('body').keypress(function(e) {

      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      }
      else {
        $body.addClass('search-active');
        $('.insta-search').removeClass('hide');
        $('.insta-searchbox').focus();
        $('.user-block__menu').addClass('hide');
        $('.partners-block__menu').addClass('hide');
        $('.partners-menu').removeClass('hello');
        $('.search').addClass('search__open');
      }
    });
  }

  $searchIcon.click(function() {
    $('.insta-search').toggleClass('hide');
    $('.insta-searchbox').focus();
    $('.user-block__menu').addClass('hide');
    $('.partners-block__menu').addClass('hide');
    $('.partners-menu').removeClass('hello');
    $('.search').toggleClass('search__open');
  });

  $instaSearchForm.submit(function( event ) {

    var query = $searchForm.val();

    if(query == "123456789") {
      $('.filters').removeClass('hide');
      $('.loading-wrapper').removeClass('hide');
      $('.dashboard').addClass('hide');
      $('.cancel-small').removeClass('hide');
      $('.page-results').addClass('hide');
      $('.page-no-results').addClass('hide');
      $('.header_search_wrap').css('height', '12rem');
      $('.page-results').css('margin-top', '13rem');
      $('.insta-search-bar').addClass('search-bar__results');
      $('.insta-search').css('background-color', 'rgba(255, 255, 255, 1)');
      $('.insta-search__label').addClass('hide');

      $('.insta-search .page-results').css('margin-top', '0');

      setTimeout( function(){
        $('.page-results').removeClass('hide');
        $('.loading-wrapper').addClass('hide');
      }  , 2000 );
    }
    else {
      $('.filters').removeClass('hide');
      $('.loading-wrapper').removeClass('hide');
      $('.dashboard').addClass('hide');
      $('.cancel-small').removeClass('hide');
      $('.page-results').addClass('hide');
      $('.page-no-results').addClass('hide');
      $('.header_search_wrap').css('height', '12rem');
      $('.insta-search').css('background-color', 'rgba(255, 255, 255, 1)');
      $('.insta-search-bar').addClass('search-bar__results');
      $('.insta-search__label').addClass('hide');

      $('.insta-search .page-results').css('margin-top', '0');

      setTimeout( function(){
        $('.page-no-results').removeClass('hide');
        $('.loading-wrapper').addClass('hide');
      }  , 2000 );
    }

    event.preventDefault();
  });

  $searchBtn.click(function() {
    var query = $searchForm.val();

    if(query == "123456789") {
      $('.filters').removeClass('hide');
      $('.loading-wrapper').removeClass('hide');
      $('.dashboard').addClass('hide');
      $('.cancel-small').removeClass('hide');
      $('.page-results').addClass('hide');
      $('.page-no-results').addClass('hide');
      $('.header_search_wrap').css('height', '12rem');
      $('.page-results').css('margin-top', '13rem');
      $('.insta-search-bar').addClass('search-bar__results');
      $('.insta-search').css('background-color', 'rgba(255, 255, 255, 1)');
      $('.insta-search__label').addClass('hide');

      $('.insta-search .page-results').css('margin-top', '0');

      setTimeout( function(){
        $('.page-results').removeClass('hide');
        $('.loading-wrapper').addClass('hide');
      }  , 2000 );
    }
    else {
      $('.filters').removeClass('hide');
      $('.loading-wrapper').removeClass('hide');
      $('.dashboard').addClass('hide');
      $('.cancel-small').removeClass('hide');
      $('.page-results').addClass('hide');
      $('.page-no-results').addClass('hide');
      $('.header_search_wrap').css('height', '12rem');
      $('.insta-search').css('background-color', 'rgba(255, 255, 255, 1)');
      $('.insta-search-bar').addClass('search-bar__results');
      $('.insta-search__label').addClass('hide');

      $('.insta-search .page-results').css('margin-top', '0');

      setTimeout( function(){
        $('.page-no-results').removeClass('hide');
        $('.loading-wrapper').addClass('hide');
      }  , 2000 );
    }
  });

  $backButton.click(function() {
    $('.filters').addClass('hide');
    $('.dashboard').removeClass('hide');
    $('.search').removeClass('hide');
    $('.cancel-small').addClass('hide');
    $('.page-results').addClass('hide');
    $('.header_search_wrap').css({'height': '7rem'});
    $('#searchbox').val('');
    $('.btn-search').addClass('hide');
    $('.searchbox').css('width', '65%');
    $('.insta-search').css('background-color', 'rgba(255, 255, 255, 0.9)');
    return false;
  });

  $closeSearchBtn.click(function() {
    $body.removeClass('search-active');
    $('.insta-search').addClass('hide');
    $('.filters').addClass('hide');
    $('.search').removeClass('hide');
    $('.page-results').addClass('hide');
    $('#searchbox').val('');
    $('.cancel-small').addClass('hide');
    $('.btn-search').addClass('hide');
    $('.insta-search-bar').removeClass('search-bar__results');
    $('.header_search_wrap').css('height', '116px');
    $('.insta-search').css('background-color', 'rgba(255, 255, 255, 0.9)');
    $('.search').removeClass('search__open');
    $('.page-no-results').addClass('hide');
    $('.dashboard').removeClass('hide');
    return false;
  });

  $closeInstaSearch.click(function() {
    $body.removeClass('search-active');
    $('.insta-search').addClass('hide');
    $('.filters').addClass('hide');
    $('.search').removeClass('hide');
    $('.page-results').addClass('hide');
    $('#searchbox').val('');
    $('.cancel-small').addClass('hide');
    $('.btn-search').addClass('hide');
    $('.insta-search-bar').removeClass('search-bar__results');
    $('.header_search_wrap').css('height', '116px');
    $('.insta-search').css('background-color', 'rgba(255, 255, 255, 0.9)');
    $('.search').removeClass('search__open');
    $('.page-no-results').addClass('hide');
    $('.dashboard').removeClass('hide');
    return false;

  });

  $(document).keyup(function(e) {
    if (e.keyCode == 27) {
      $body.removeClass('search-active');
      $('.insta-search').addClass('hide');
      $('.filters').addClass('hide');
      $('.search').removeClass('hide');
      $('.page-results').addClass('hide');
      $('#searchbox').val('');
      $('.cancel-small').addClass('hide');
      $('.btn-search').addClass('hide');
      $('.dashboard').removeClass('hide');
      $('.insta-search-bar').removeClass('search-bar__results');
      $('.header_search_wrap').css('height', '116px');
      $('.insta-search').css('background-color', 'rgba(255, 255, 255, 0.9)');
      $('.search').removeClass('search__open');
      $('.page-no-results').addClass('hide');
      return false;
    }
  });

});
