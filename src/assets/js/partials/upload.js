$(function(){

  let $uploadfile1 = $('#upload-file1'),
      $uploadfile2 = $('#upload-file2');

  $uploadfile1.click(function() {
    $('.labels').removeClass('hide');
    $('#checkboxc').prop('checked', true);
    $('#checkboxe').prop('checked', false);
    $('#upload-file1').css('border-color', '#d13528');
    $('#upload-file2').css('border-color', 'transparent');
    return false;
  });

  $uploadfile2.click(function() {
    $('.labels').removeClass('hide');
    $('#checkboxe').prop('checked', true);
    $('#checkboxc').prop('checked', false);
    $('#upload-file1').css('border-color', 'transparent');
    $('#upload-file2').css('border-color', '#d13528');
    return false;
  });
});
