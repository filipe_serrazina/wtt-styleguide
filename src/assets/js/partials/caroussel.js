$(function(){
  $('.slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    nextArrow: '<button type="button" class="slider-arrow slider-next">Next</button>',
    prevArrow: '<button type="button" class="slider-arrow slider-prev">Previous</button>',
    responsive: [
      {
        breakpoint: 1400,
        settings: {
          arrows: false,
          center: true,
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1023,
        settings: {
          arrows: false,
          center: true,
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          dots: true,
          center: true,
          slidesToShow: 1
        }
      },
    ]
  });
});
