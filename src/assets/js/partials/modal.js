$(function(){
  let $radioInput = $('.ws-form input[type="radio"]'),
      $formButton = $('.ws-form .ws-main-btn');

  $radioInput.click(function() {
    $formButton.removeClass('btn-disabled').addClass('btn-primary').removeAttr('disabled');
  });
});
