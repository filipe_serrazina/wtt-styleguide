
$(function(){
  // Scroll to Anchor
  let $siteNavAnchor = $('.w-nav-tabs a');

  $siteNavAnchor.on('click', function(event) {

    let target = $(this.getAttribute('href').replace(/^.*?(#|$)/,'#') );

    if( target.length ) {

      event.preventDefault();

      $('html, body').stop().animate({
        scrollTop: target.offset().top - 320
      }, 1000);
    }
  });

  let $url = window.location.pathname.split('/').pop();

  // Scroll on Page Load
  if ($url == "service-page.html") {
    $('html, body').animate({ scrollTop: 1100 }, 1000);
  }

  if ($url == "2-new-client-contract.html") {
    $('html, body').animate({ scrollTop: 1100 }, 1000);
  }

  if ($url == "2-new-client-contract-first.html") {
    $('html, body').animate({ scrollTop: 1100 }, 1000);
  }

  if ($url == "3-client-contract.html") {
    $('html, body').animate({ scrollTop: 1100 }, 1000);
  }

  if ($url == "new-service.html") {
    $('html, body').animate({ scrollTop: 1100 }, 1000);
  }
});
