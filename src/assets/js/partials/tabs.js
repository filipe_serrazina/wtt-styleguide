$(function(){

  let $serviceForm    = $('#service-form'),
      $serviceData    = $('#service-data'),
      $serviceInfo    = $('#service-info'),
      $serviceAttach  = $('#service-attach'),
      $serviceLeads   = $('#service-lead'),
      $serviceOffers  = $('#service-offers'),
      $serviceHistory = $('#service-history'),
      $siteNavAnchor  = $('.w-nav-tabs a');

  $serviceForm.waypoint(function() {
    $('.user-tab').toggleClass('user-tab__close');
    $('.header').toggleClass('hide');
    $('.header').addClass('header-fadein');
  }, {
    offset: '45%'
  });

  $serviceData.waypoint(function() {
    $('.user-tab').toggleClass('user-tab__close');
    $siteNavAnchor.removeClass('active');
    $('#client-2').addClass('active');
    $('#service-header-double').css('visibility', 'visible');
  }, {
    offset: '-80%'
  });

  $serviceInfo.waypoint(function() {
    $siteNavAnchor.removeClass('active');
    $('.header').toggleClass('hide');
    $('.header').addClass('header-fadein');
    $('#client-2').addClass('active');
    $('#service-header-double').css('visibility', 'hidden');
  }, {
    offset: 320
  });

  $serviceOffers.waypoint(function() {
    $siteNavAnchor.removeClass('active');
    $('#client-3').addClass('active');
  }, {
    offset: 320
  });

  $serviceAttach.waypoint(function() {
    $siteNavAnchor.removeClass('active');
    $('#client-4').addClass('active');
  }, {
    offset: 320
  });

  $serviceLeads.waypoint(function() {
    $siteNavAnchor.removeClass('active');
    $('#client-5').addClass('active');
  }, {
    offset: 320
  });

  $serviceHistory.waypoint(function() {
    $siteNavAnchor.removeClass('active');
    $('#client-6').addClass('active');
  }, {
    offset: 320
  });
});
