
$(function(){

  let $checkbox3 = $('#checkbox3'),
      $checkbox4 = $('#checkbox4'),
      $checkbox5 = $('#checkbox5');

  $checkbox3.change(function() {
    $('.phone-data').toggleClass('hide');
  });

  $checkbox4.change(function() {
    $('.mobile-data').toggleClass('hide');
  });

  $checkbox5.change(function() {
    $('.net-data').toggleClass('hide');
  });
});
