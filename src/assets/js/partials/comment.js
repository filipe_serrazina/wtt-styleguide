
$(function(){

  let $editcomment1 = $('#edit-comment-1'),
      $commentcancel1 = $('#comment-cancel1'),
      $commentsave1 = $('#comment-save1'),
      $editcomment1c = $('#edit-comment-1c'),
      $commentcancel1c = $('#comment-cancel1c'),
      $commentsave1c = $('#comment-save1c'),
      $editcomment2 = $('#edit-comment-2'),
      $commentcancel2 = $('#comment-cancel2'),
      $commentsave2 = $('#comment-save2');

  $editcomment1.click(function() {
    $('#comment_wrapper1').removeClass('hide');
    $('#form-data1').addClass('hide');
    $('.form-edit').addClass('hide');
    $('.form-delete').addClass('hide');
    $('#comment-textarea1').focus();
    return false;
  });

  $commentsave1.click(function() {
    $('#comment_wrapper1').addClass('hide');
    $('#form-data1').removeClass('hide');
    $('.form-edit').removeClass('hide');
    $('.form-delete').removeClass('hide');
    return false;
  });

  $commentcancel1.click(function() {
    $('#comment_wrapper1').addClass('hide');
    $('#form-data1').removeClass('hide');
    $('.form-edit').removeClass('hide');
    $('.form-delete').removeClass('hide');
    return false;
  });

  $editcomment2.click(function() {
    $('#comment_wrapper2').removeClass('hide');
    $('#form-data2').addClass('hide');
    $('.form-edit').addClass('hide');
    $('.form-delete').addClass('hide');
    $('#form-file2').addClass('hide');
    $('.comment-textarea2').focus();
    return false;
  });

  $commentsave2.click(function() {
    $('#comment_wrapper2').addClass('hide');
    $('#form-data2').removeClass('hide');
    $('.form-edit').removeClass('hide');
    $('.form-delete').removeClass('hide');
    $('#form-file2').removeClass('hide');
    return false;
  });

  $commentcancel2.click(function() {
    $('#comment_wrapper2').addClass('hide');
    $('#form-data2').removeClass('hide');
    $('.form-edit').removeClass('hide');
    $('.form-delete').removeClass('hide');
    $('#form-file2').removeClass('hide');
    return false;
  });

$editcomment1c.click(function() {
    $('#comment_wrapper1c').removeClass('hide');
    $('#form-data1c').addClass('hide');
    $('.form-edit').addClass('hide');
    $('.form-delete').addClass('hide');
    $('#comment-textarea1c').focus();
    return false;
  });

  $commentsave1c.click(function() {
    $('#comment_wrapper1c').addClass('hide');
    $('#form-data1c').removeClass('hide');
    $('.form-edit').removeClass('hide');
    $('.form-delete').removeClass('hide');
    return false;
  });

  $commentcancel1c.click(function() {
    $('#comment_wrapper1c').addClass('hide');
    $('#form-data1c').removeClass('hide');
    $('.form-edit').removeClass('hide');
    $('.form-delete').removeClass('hide');
    return false;
  });
});
