
$(function(){
  let $tableRow = $('.clickable-row');

  $tableRow.click(function() {
    window.location = $(this).data('href');
  });
});
