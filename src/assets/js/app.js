import $ from 'jquery';
import whatInput from 'what-input';
import libs from './lib/dependencies';

window.$ = $;
window.libs = libs;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
import './lib/jquery.waypoints';

import './partials/anchors';
import './partials/caroussel';
import './partials/fixed-bar';
import './partials/menu';
import './partials/modal';
import './partials/searchform';
import './partials/tables';
import './partials/tabs';
import './partials/comment';
import './partials/upload';
import './partials/form';

import './partials/pages/service';

$(function(){

});

$(document).foundation();
